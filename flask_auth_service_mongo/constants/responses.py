# HTTP messages
BAD_REQUEST = 'bad_request'
OK = 'ok'
UNAUTHORIZED = 'unauthorized'
CREATED = 'created'
NOT_ACCEPTABLE = 'not_acceptable'
NOT_FOUND = 'not_found'
INTERNAL_SERVER_ERROR = 'internal_server_error'

# Actions
SAVED = 'saved'

# Messages
BAD_CREDENTIALS = 'bad_credentials'
SIGNATURE_EXPIRED = 'signature_expired'
INVALID_TOKEN = 'invalid_token'
ERROR = 'error'
EXISTING_RESOURCE = 'existing_resource'
ID_NOT_VALID = 'id_not_valid'
